package flight.booking.service;

import flight.booking.model.*;
import flight.booking.repository.FlightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ScheduledFlightServiceTest {
    FlightService service ;
    ArrayList<ScheduledFlight> res = new ArrayList <ScheduledFlight> (  );
    SearchCriteria searchCriteria = new SearchCriteria ();

    @Mock
    FlightRepository mockrepository = mock( FlightRepository.class);;
    //FlightBookingForm mockflightForm = mock( FlightBookingForm.class );
    Date mockDate = mock( Date.class);

    @BeforeEach
    void setUp () {
        TravelClass economy_air_319 = new TravelClass ( "ECONOMY",144,4000.0,144 );
        TravelClass business_boeing = new TravelClass ("BUSINESS",35,13000.00,35);
        TravelClass economy_boeing = new TravelClass ( "ECONOMY",195,6000.0,195);
        TravelClass first_boeing = new TravelClass ( "FIRST",8,20000.0 ,8);
        TravelClass business_air_321 = new TravelClass ("BUSINESS",20,10000.0,20);
        TravelClass economy_air_321 = new TravelClass ( "ECONOMY",152,5000.0,152 );
        Map<String,TravelClass> travelClassModelMap__air_319 = new HashMap <String,TravelClass> (  );
        travelClassModelMap__air_319.put ("ECONOMY",economy_air_319  );
        Map<String,TravelClass> travelClassModelMap__boeing = new HashMap <String,TravelClass> (  );
        travelClassModelMap__boeing.put ("BUSINESS",business_boeing  );
        travelClassModelMap__boeing.put ("ECONOMY",economy_boeing  );
        travelClassModelMap__boeing.put ("FIRST",first_boeing  );
        Map<String,TravelClass> travelClassModelMap__air_321 = new HashMap <String,TravelClass> (  );
        travelClassModelMap__air_321.put ("BUSINESS",business_air_321  );
        travelClassModelMap__air_321.put ("ECONOMY",economy_air_321  );

        Map<String,Integer> travelClassSeatDistributionMap_AIRBUS_319 = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_AIRBUS_319.put ( TravelClassEnum.ECONOMY.toString (),144 );
        Map<String,Integer> travelClassSeatDistributionMap_BOEING = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.BUSINESS.toString (),35 );
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.ECONOMY.toString (),195 );
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.FIRST.toString (),8 );
        Map<String,Integer> travelClassSeatDistributionMap_AIRBUS_321 = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_AIRBUS_321.put ( TravelClassEnum.BUSINESS.toString (),20 );
        travelClassSeatDistributionMap_AIRBUS_321.put ( TravelClassEnum.ECONOMY.toString (),152 );

        Aeroplane AIRBUS_319=new Aeroplane ("AIRBUS_319",travelClassSeatDistributionMap_AIRBUS_319,144);
        Aeroplane BOEING=new Aeroplane ("BOEING_777",travelClassSeatDistributionMap_BOEING,238);
        Aeroplane AIRBUS_A321=new Aeroplane ("AIRBUS_321",travelClassSeatDistributionMap_AIRBUS_321,172);



        FlightRoute flightRoute_D_H = new FlightRoute ( "R1","DELHI","HYDERABAD", makeDate() );
        FlightRoute flightRoute_H_D = new FlightRoute ( "R2","HYDERABAD","DELHI", makeDateFromString ("06/09/2019" ));
        FlightRoute flightRoute_G_K =new FlightRoute ("R3", "GOA","KOCHIN", makeDateFromString ("03/09/2019" ));
        FlightRoute flightRoute_K_G = new FlightRoute ("R4", "KOCHIN","GOA",makeDateFromString ("01/09/2019" ) );
        FlightRoute flightRoute_C_H = new FlightRoute ( "R5","CHENNAI","HYDERABAD",makeDateFromString ("25/08/2019" ));
        FlightRoute flightRoute_H_C = new FlightRoute ( "R6","HYDERABAD","CHENNAI",makeDateFromString ("25/08/2019" ));
        FlightRoute flightRoute_M_C = new FlightRoute ( "R7","MUMBAI","CHENNAI",makeDateFromString ("25/08/2019" ));
        FlightRoute flightRoute_C_M = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("05/09/2019" ));
        FlightRoute flightRoute_C_M_1 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("04/09/2019" ));
        FlightRoute flightRoute_C_M_2 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("03/09/2019" ));
        FlightRoute flightRoute_C_M_3 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("02/09/2019" ));
        FlightRoute flightRoute_C_M_7 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("01/09/2019" ));
        FlightRoute flightRoute_C_M_4 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("31/08/2019"));
        FlightRoute flightRoute_C_M_5 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("30/08/2019"));
        FlightRoute flightRoute_C_M_8 = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("29/08/2019"));


        ScheduledFlight f1 =  new ScheduledFlight ("INDIGO001", "INDIGO",
                flightRoute_D_H , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f2 =  new ScheduledFlight ("INDIGO002","INDIGO",
                flightRoute_H_D , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f3 =  new ScheduledFlight ("INDIGO002","INDIGO",
                flightRoute_G_K , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f4 =  new ScheduledFlight ("INDIGO003","INDIGO",
                flightRoute_C_M , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f5 =  new ScheduledFlight ("INDIGO004","INDIGO",
                flightRoute_C_M_1 , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f6 =  new ScheduledFlight ("INDIGO005","INDIGO",
                flightRoute_C_M_2 , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f7 =  new ScheduledFlight ("INDIGO006","INDIGO",
                flightRoute_C_M_3 , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f8 =  new ScheduledFlight ("INDIGO007","INDIGO",
                flightRoute_C_M_4 , BOEING,travelClassModelMap__boeing);
        ScheduledFlight f9 =  new ScheduledFlight ("INDIGO007","INDIGO",
                flightRoute_C_M_5 , BOEING,travelClassModelMap__boeing);


        service = new FlightService ( mockrepository );
        res.add ( f1 );
        res.add ( f2 );
        res.add ( f3);
        res.add ( f4 );
        res.add ( f5 );
        res.add ( f6 );
        res.add ( f7 );
        res.add ( f8 );
        res.add ( f9 );
    }


    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_EMPTY_Passenger() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel ( makeDate());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println ();
        assertEquals ( 0,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_EMPTY_Destination_Passenger() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel ( makeDate());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println ();
        assertEquals ( 0,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_Present() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel ( makeDate());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals ( 1,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NOTINDB_Passenger_1() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("RRR");
        searchCriteria.setDestinationName ("YYY");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel ( makeDate());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println ();
        assertEquals ( 0,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NO_DATE_GIVEN_Passenger_1() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel (makeDateFromString ( "" ));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println ();
        assertEquals ( 1,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_TRAVELCLASSNOTCHOSEN() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println ();
        assertEquals ( 1,result.size () );

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_0() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println (result.get ( 0 ).getNumberOfPassengerTravelling ());
        assertEquals (result.get ( 0 ).getNumberOfPassengerTravelling (),1);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_Test_TICKETCOST_ECONOMY_Traveller_1() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println (result.get ( 0 ).getNumberOfPassengerTravelling ());
        assertEquals (result.get ( 0 ).getTicketCost (),6000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_Test_TICKETCOST_ECONOMY_Traveller_4() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (4);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("ECONOMY" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        System.out.println (result.get ( 0 ).getNumberOfPassengerTravelling ());
        assertEquals (result.get ( 0 ).getTicketCost (),24000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_Test_TICKETCOST_BUSINESS_MONDAY_Traveller_1() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("BUSINESS" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),18200.0,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_Test_TICKETCOST_BUSINESS_MONDAY_Traveller_4 (){
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (4);
        searchCriteria.setSourceName ("DELHI");
        searchCriteria.setDestinationName ("HYDERABAD");
        searchCriteria.setTravelClass ("BUSINESS" );
        searchCriteria.setDateOfTravel (makeDate ());
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),72800.0,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_SourceAndDestination_NoOfPassenger_Test_TICKETCOST_BUSINESS_NONMONDAY() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("GOA");
        searchCriteria.setDestinationName ("KOCHIN");
        searchCriteria.setTravelClass ("BUSINESS" );
        searchCriteria.setDateOfTravel (makeDateFromString ( "03/09/2019" ));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),13000.0,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_1_DateDiff_9() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (1);
        searchCriteria.setSourceName ("HYDERABAD");
        searchCriteria.setDestinationName ("DELHI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("06/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),22000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_4_DateDiff_10() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (4);
        searchCriteria.setSourceName ("HYDERABAD");
        searchCriteria.setDestinationName ("DELHI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("06/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),88000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_10() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("HYDERABAD");
        searchCriteria.setDestinationName ("DELHI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("06/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),22000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_9() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("05/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),24000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_8() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("04/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),26000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_7() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("03/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),28000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_6() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("02/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),30000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_5() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("01/09/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),32000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_4() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("31/08/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),32000.00,0.0);

    }
    @Test
    public void searchFlightBasedOnSourceAndDestinationNew_Test_Source_Destination_NoOfPassenger_Test_TICKETCOST_FIRST_Traveller_0_DateDiff_3() {
        when ( mockrepository.getFlights ()).thenReturn (res);
        searchCriteria.setNumberOfPassengers (0);
        searchCriteria.setSourceName ("CHENNAI");
        searchCriteria.setDestinationName ("MUMBAI");
        searchCriteria.setTravelClass ("FIRST" );
        searchCriteria.setDateOfTravel (makeDateFromString ("30/08/2019"));
        ArrayList <ScheduledFlights_ResultSet> result =  service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        assertEquals (result.get ( 0 ).getTicketCost (),34000.00,0.0);

    }
    public static Date makeDate ( ) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ( pattern );
        Date inDate = new Date (  );
        String newDate = simpleDateFormat.format ( inDate );
        Date result;
        try {
            result = simpleDateFormat.parse ( newDate );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }
    public static Date makeDateFromString(String str){
        Date date1= null;
        try {
            if(str.equalsIgnoreCase ( "" )) {
                date1 = makeDate ();
            }
            else{
                date1 = new SimpleDateFormat ("dd/MM/yyyy").parse(str);
            }
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        System.out.println(str+"\t"+date1);
        return date1;

    }
}
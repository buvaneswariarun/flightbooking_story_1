package flight.booking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlightBookingMainApp {

    public static void main ( String[] args ) {
        SpringApplication.run ( FlightBookingMainApp.class , args );
    }
}
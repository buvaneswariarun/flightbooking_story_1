package flight.booking.repository;

import flight.booking.model.*;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class FlightRepository {


    public ArrayList<String> sourceRepository;
    public ArrayList<String> destinationRepository;
    public Map <String, ScheduledFlight> repository;
    public ArrayList<String> travelClassRepository;

    public FlightRepository () {
        this.repository = new HashMap <String, ScheduledFlight> ();
        this.sourceRepository = new ArrayList <String>(  );
        this.destinationRepository = new ArrayList <String>(  );
        this.loadSource();
        this.loadDestinations();
        this.loadFLights ();
    }
    public void loadTravelClass(){
        this.travelClassRepository = new ArrayList <String> (  );
        EnumSet.allOf(TravelClassEnum.class)
                .forEach(TravelClassEnum -> this.travelClassRepository.add ( TravelClassEnum.toString () ));
    }

    public void loadSource(){
        this.sourceRepository.add("DELHI" );
        this.sourceRepository.add("HYDERABAD" );
        this.sourceRepository.add("GOA" );
        this.sourceRepository.add("KOCHIN" );
        this.sourceRepository.add("CHENNAI" );
        this.sourceRepository.add("MUMBAI" );
    }

    public void loadDestinations(){

        this.destinationRepository.add("DELHI" );
        this.destinationRepository.add("HYDERABAD" );
        this.destinationRepository.add("GOA" );
        this.destinationRepository.add("KOCHIN" );
        this.destinationRepository.add("CHENNAI" );
        this.destinationRepository.add("MUMBAI" );

    }
    public void loadFLights ()  {


        TravelClass economy_air_319 = new TravelClass ( "ECONOMY",144,4000.0,144 );
        TravelClass business_boeing = new TravelClass ("BUSINESS",35,13000.00,35);
        TravelClass economy_boeing = new TravelClass ( "ECONOMY",195,6000.0,195);
        TravelClass first_boeing = new TravelClass ( "FIRST",8,20000.0 ,8);
        TravelClass business_air_321 = new TravelClass ("BUSINESS",20,10000.0,20);
        TravelClass economy_air_321 = new TravelClass ( "ECONOMY",152,5000.0,152 );


        Map<String,TravelClass> travelClassModelMap__air_319 = new HashMap <String,TravelClass> (  );
        travelClassModelMap__air_319.put ("ECONOMY",economy_air_319  );
        Map<String,TravelClass> travelClassModelMap__boeing = new HashMap <String,TravelClass> (  );
        travelClassModelMap__boeing.put ("BUSINESS",business_boeing  );
        travelClassModelMap__boeing.put ("ECONOMY",economy_boeing  );
        travelClassModelMap__boeing.put ("FIRST",first_boeing  );
        Map<String,TravelClass> travelClassModelMap__air_321 = new HashMap <String,TravelClass> (  );
        travelClassModelMap__air_321.put ("BUSINESS",business_air_321  );
        travelClassModelMap__air_321.put ("ECONOMY",economy_air_321  );

        Map<String,Integer> travelClassSeatDistributionMap_AIRBUS_319 = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_AIRBUS_319.put ( TravelClassEnum.ECONOMY.toString (),144 );
        Map<String,Integer> travelClassSeatDistributionMap_BOEING = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.BUSINESS.toString (),35 );
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.ECONOMY.toString (),195 );
        travelClassSeatDistributionMap_BOEING.put ( TravelClassEnum.FIRST.toString (),8 );
        Map<String,Integer> travelClassSeatDistributionMap_AIRBUS_321 = new HashMap <String, Integer> ();
        travelClassSeatDistributionMap_AIRBUS_321.put ( TravelClassEnum.BUSINESS.toString (),20 );
        travelClassSeatDistributionMap_AIRBUS_321.put ( TravelClassEnum.ECONOMY.toString (),152 );

        Aeroplane AIRBUS_319=new Aeroplane ("AIRBUS_319",travelClassSeatDistributionMap_AIRBUS_319,144);
        Aeroplane BOEING=new Aeroplane ("BOEING_777",travelClassSeatDistributionMap_BOEING,238);
        Aeroplane AIRBUS_A321=new Aeroplane ("AIRBUS_321",travelClassSeatDistributionMap_AIRBUS_321,172);

        FlightRoute flightRoute_D_H = new FlightRoute ( "R1","DELHI","HYDERABAD", makeDateFromString ("30/08/2019") );
        FlightRoute flightRoute_H_D = new FlightRoute ( "R2","HYDERABAD","DELHI", makeDateFromString ("02/09/2019" ));
        FlightRoute flightRoute_G_K =new FlightRoute ("R3", "GOA","KOCHIN", makeDateFromString ("03/09/2019" ));
        FlightRoute flightRoute_K_G = new FlightRoute ("R4", "KOCHIN","GOA",makeDateFromString ("07/09/2019" ) );
        FlightRoute flightRoute_C_H = new FlightRoute ( "R5","CHENNAI","HYDERABAD",makeDateFromString ("29/08/2019" ));
        FlightRoute flightRoute_H_C = new FlightRoute ( "R6","HYDERABAD","CHENNAI",makeDateFromString ("29/08/2019" ));
        FlightRoute flightRoute_M_C = new FlightRoute ( "R7","MUMBAI","CHENNAI",makeDateFromString ("24/08/2019" ));
        FlightRoute flightRoute_C_M = new FlightRoute ( "R8","CHENNAI","MUMBAI",makeDateFromString ("07/09/2019" ));


        repository.put ( "INDIGO001" , new ScheduledFlight ("INDIGO001", "INDIGO", flightRoute_D_H , AIRBUS_319,travelClassModelMap__air_319));

        repository.put ( "SPICEJET002",new ScheduledFlight ("SPICEJET002", "SPICEJET", flightRoute_H_D , BOEING,travelClassModelMap__boeing) );

        repository.put ( "AIRINDIA003",new ScheduledFlight ("AIRINDIA003", "AIRINDIA", flightRoute_D_H , AIRBUS_A321,travelClassModelMap__air_321) );

        repository.put ( "INDIGO_004" ,new ScheduledFlight ("INDIGO_004", "INDIGO", flightRoute_C_M , AIRBUS_319,travelClassModelMap__air_319) );

        repository.put ( "INDIGO_004",new ScheduledFlight ("SPICEJET_005","SPICEJET", flightRoute_K_G , BOEING,travelClassModelMap__boeing) );

        repository.put ( "CARR_006",new ScheduledFlight ("AIRINDIA006","AIRINDIA", flightRoute_H_C, AIRBUS_A321,travelClassModelMap__air_321) );

        repository.put ( "CARR_007" ,new ScheduledFlight ("TRUEJET007","TRUEJET", flightRoute_C_H , AIRBUS_319,travelClassModelMap__air_319) );

        repository.put ( "CARR_008",new ScheduledFlight ("INDIGO008","INDIGO", flightRoute_M_C , BOEING,travelClassModelMap__boeing) );

        repository.put ( "CARR_009", new ScheduledFlight ("SPICEJET009","SPICEJET", flightRoute_M_C , AIRBUS_A321,travelClassModelMap__air_321) );

        repository.put ( "CARR_010", new ScheduledFlight ("INDIGO010","INDIGO", flightRoute_H_D , BOEING,travelClassModelMap__boeing));
    }

    ArrayList <ScheduledFlight> listOfFlights () {
        loadFLights ();
        return new ArrayList <ScheduledFlight> ( repository.values () );
    }


    public ArrayList <ScheduledFlight> getFlights () {
        loadFLights ();
        return listOfFlights ();
    }

    public ScheduledFlight retrieve ( String flightNumber ) {
        loadFLights ();
        return repository.get ( flightNumber );
    }


    public static Date makeDateFromString(String str){
        Date date1= null;
        try {
            date1 = new SimpleDateFormat ("dd/MM/yyyy").parse(str);
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        System.out.println(str+"\t"+date1);
        return date1;

    }
}
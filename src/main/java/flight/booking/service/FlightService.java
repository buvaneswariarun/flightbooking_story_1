package flight.booking.service;

import flight.booking.model.ScheduledFlight;
import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlights_ResultSet;
import flight.booking.model.TravelClassEnum;
import flight.booking.repository.FlightRepository;
import flight.booking.service.helper.FlightHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class FlightService {
    @Autowired
    private FlightRepository repository;

    public FlightService ( FlightRepository repository ) {
        this.repository = repository;
    }

    public FlightService () {

    }

    public ArrayList <ScheduledFlight> searchFlight () {

        return this.repository.getFlights ();
    }

    public ScheduledFlight getFlightByFlightNumber ( String flightNumber ) {

        return this.repository.retrieve ( flightNumber );
    }

    public ArrayList <ScheduledFlights_ResultSet> searchFlightBasedOnSourceAndDestinationNew ( SearchCriteria searchCriteria ) {
        FlightHandler handler = new FlightHandler ();
        Collection <ScheduledFlight> scheduledFlights = this.repository.getFlights ();
        ArrayList <ScheduledFlights_ResultSet> flightList = new ArrayList <ScheduledFlights_ResultSet> ();
        if (searchCriteria.getNumberOfPassengers () == 0) {
            searchCriteria.setNumberOfPassengers ( 1 );
        }
        if(searchCriteria.getTravelClass ().equalsIgnoreCase ( "" )){
            searchCriteria.setTravelClass ( TravelClassEnum.ECONOMY.toString () );
        }
        if(searchCriteria.getDateOfTravel () !=null) {
            if (searchCriteria.getDateOfTravel ().toString ().equalsIgnoreCase ( "" )) {
                searchCriteria.setTravelClass ( new Date ().toString () );
            }
        }else{
            searchCriteria.setTravelClass ( new Date ().toString () );
        }
        for (ScheduledFlight scheduledFlight : scheduledFlights) {
            if (handler.checkWhetherSourceDestinationMatches ( scheduledFlight , searchCriteria )) {
                if (handler.checkIfTravelDateMatchesWithDateOfJourney ( scheduledFlight , searchCriteria )) {
                    if (handler.checkIfTravelClassMatches ( scheduledFlight.getAeroplane ().getTravelClassModelMap (), searchCriteria )) {
                        ScheduledFlights_ResultSet resultFlight = new ScheduledFlights_ResultSet ();
                        resultFlight.setFlightDepartureDate ( scheduledFlight.getFlightRoute ().getDepartureDate () );
                        resultFlight.setSourceName ( scheduledFlight.getFlightRoute ().getSourceName () );
                        resultFlight.setDestinationName ( scheduledFlight.getFlightRoute ().getDestinationName () );
                        resultFlight.setFlightNumber ( scheduledFlight.getFlightNumber () );
                        resultFlight.setTravelClass ( searchCriteria.getTravelClass () );
                        resultFlight.setAvailableSeats ( handler.fetchAvailableSeatForGivenTravelClass ( scheduledFlight.getAeroplane ().getTravelClassModelMap () , searchCriteria ) );
                        resultFlight.setTicketCost ( handler.calculateTicketCost ( scheduledFlight, searchCriteria ) );
                        resultFlight.setNumberOfPassengerTravelling ( searchCriteria.getNumberOfPassengers () );
                        flightList.add ( resultFlight );
                    }
                }
            }
        }
        return flightList;
    }
    public ArrayList<String> getSources(){
        return (repository.sourceRepository);

    }
    public ArrayList<String> getDestinations(){
        return (repository.destinationRepository);

    }
}


package flight.booking.service;

import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlight;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BusinessClassTicketCostCalculator implements TicketCostCalculator {
    @Override
    public double calculateTicketCost ( ScheduledFlight flight , SearchCriteria form ) {
        {
            int noOfTravellers = form.getNumberOfPassengers ();
            double ticketCost = 0.0;
            double basePrice = flight.getTravelClassSeatDistributionMap ().get ( form.getTravelClass () ).getBasePrice ();
            double raisedFare = 0.0;
            Date dateOfJourney = form.getDateOfTravel ();
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
            String dayOfTheWeek = simpleDateformat.format ( dateOfJourney );
            for(int i=0;i<noOfTravellers;i++) {
                if (dayOfTheWeek.equalsIgnoreCase ( "MONDAY" ) ||
                        (dayOfTheWeek.equalsIgnoreCase ( "WEDNESDAY" )) ||
                        (dayOfTheWeek.equalsIgnoreCase ( "FRIDAY" ))) {
                    raisedFare = (basePrice + (basePrice * .40));
                }else{
                    raisedFare = basePrice;
                }
                ticketCost = raisedFare * noOfTravellers;
            }
            return ticketCost;
        }
    }
}

package flight.booking.service.helper;

import flight.booking.model.ScheduledFlight;
import flight.booking.model.SearchCriteria;
import flight.booking.model.TravelClassEnum;
import flight.booking.service.BusinessClassTicketCostCalculator;
import flight.booking.service.EconomyClassTicketCostCalculator;
import flight.booking.service.FirstClassTicketCostCalculator;
import flight.booking.service.TicketCostCalculator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class FlightHandler {

    public boolean checkIfTravelDateMatchesWithDateOfJourney ( ScheduledFlight scheduledFlight , SearchCriteria searchCriteria ) {
        boolean result = false;
        Date d1 = null;
       if(searchCriteria.getDateOfTravel () !=null) {
           if (searchCriteria.getDateOfTravel ().toString ().equalsIgnoreCase ( "" )) {
               d1 = makeDate ();
           } else {
               d1 = makeDate ( searchCriteria.getDateOfTravel () );
           }
           Date d2 = makeDate ( scheduledFlight.getFlightRoute ().getDepartureDate () );
           scheduledFlight.getFlightRoute ().setDepartureDate ( d2 );
           if ((d1.compareTo ( d2 ) == 0)) {
               result = true;
           }
       }else{
           return false;
       }
        return result;
    }
    public boolean checkIfTravelClassMatches( Map<String, Integer> seatMap , SearchCriteria form){
        boolean result = false;
        if(seatMap.containsKey ( form.getTravelClass () )){
            result=true;
        }
        return result;
    }
    public int fetchAvailableSeatForGivenTravelClass ( Map<String, Integer> seatMap, SearchCriteria searchCriteria ) {
        int numberOfseats = 0;
        if ((seatMap.get( searchCriteria.getTravelClass ()) !=null)) {
            numberOfseats= seatMap.get ( searchCriteria.getTravelClass ());
        }
        return numberOfseats;
    }
    public static Date makeDate ( Date inDate ) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ( pattern );
        String newDate = simpleDateFormat.format ( inDate );
        Date result;
        try {
            result = simpleDateFormat.parse ( newDate );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public boolean checkWhetherSourceDestinationMatches( ScheduledFlight scheduledFlight , SearchCriteria form){
        boolean result = false;
        if(scheduledFlight !=null && form !=null){
            if((scheduledFlight.getFlightRoute ().getSourceName ().equalsIgnoreCase ( form.getSourceName () )) &&
                    (scheduledFlight.getFlightRoute ().getDestinationName ().equalsIgnoreCase ( form.getDestinationName () ))){
                result=true;
            }
        }
        return result;


    }
    public double calculateTicketCost(ScheduledFlight flight, SearchCriteria form){
        double ticketCost = 0.0;
        TicketCostCalculator ticketCostCalculator;
        if(form.getTravelClass ().toString ().equalsIgnoreCase ( TravelClassEnum.ECONOMY.toString () )){
            ticketCostCalculator = new EconomyClassTicketCostCalculator ();
            ticketCost= ticketCostCalculator.calculateTicketCost (flight,form);
        }else if(form.getTravelClass ().toString ().equalsIgnoreCase ( TravelClassEnum.BUSINESS.toString () )){
            ticketCostCalculator = new BusinessClassTicketCostCalculator ();
            ticketCost= ticketCostCalculator.calculateTicketCost (flight,form);
        }else{
            ticketCostCalculator = new FirstClassTicketCostCalculator ();
            ticketCost = ticketCostCalculator.calculateTicketCost (flight,form);
        }
        return ticketCost;
    }

    public static Date makeDate ( ) {
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ( pattern );
        Date inDate = new Date (  );
        String newDate = simpleDateFormat.format ( inDate );
        Date result;
        try {
            result = simpleDateFormat.parse ( newDate );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }
}

package flight.booking.service;

import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlight;

public interface TicketCostCalculator {
    public double calculateTicketCost( ScheduledFlight flight, SearchCriteria form );
}

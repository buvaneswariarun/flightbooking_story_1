package flight.booking.service;

import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlight;

public class EconomyClassTicketCostCalculator implements TicketCostCalculator {
    @Override
    public double calculateTicketCost ( ScheduledFlight flight , SearchCriteria form ) {
        {
            int noOfTravellers = form.getNumberOfPassengers ();
            int totalSeatsInEconomyClassAsPerDesign = flight.getTravelClassSeatDistributionMap ().get ( form.getTravelClass () ).getSeatsPerDesign ();
            int currentSeatAvailability = flight.getTravelClassSeatDistributionMap ().get ( form.getTravelClass () ).getAvailableSeatsForBooking ();
            int totalSeatsBooked = totalSeatsInEconomyClassAsPerDesign-currentSeatAvailability;
            int theSeatToBeBooked = 0;
            double basePrice = flight.getTravelClassSeatDistributionMap ().get ( form.getTravelClass () ).getBasePrice ();
            double elevatedPrice = 0.0;
            double totalTicketCost = 0.0;
            double noOfSeatsWhichHaveNormalBasePrice = (((totalSeatsInEconomyClassAsPerDesign) * (0.40)));
            int tenPercentCutOff = (totalSeatsInEconomyClassAsPerDesign / 100) * 90;
            int remaining = totalSeatsInEconomyClassAsPerDesign - tenPercentCutOff;

            for (int i = 0; i < noOfTravellers; i++) {
                theSeatToBeBooked = totalSeatsBooked + (i + 1);
                if (theSeatToBeBooked <= noOfSeatsWhichHaveNormalBasePrice) {
                    totalTicketCost = totalTicketCost + basePrice;
                } else if ((theSeatToBeBooked > noOfSeatsWhichHaveNormalBasePrice) && (theSeatToBeBooked <= tenPercentCutOff)) {
                    elevatedPrice = basePrice + (basePrice / 100) * 30;
                    totalTicketCost = totalTicketCost + elevatedPrice;
                } else {
                    elevatedPrice = basePrice + (basePrice / 100) * 60;
                    totalTicketCost = totalTicketCost + elevatedPrice;
                }
            }
            flight.getTravelClassSeatDistributionMap ()
                    .get ( form.getTravelClass () )
                    .setAvailableSeatsForBooking ( currentSeatAvailability- noOfTravellers);
            return totalTicketCost;
        }
    }
}

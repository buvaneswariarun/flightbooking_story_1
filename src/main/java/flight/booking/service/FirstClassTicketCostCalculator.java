package flight.booking.service;

import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlight;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FirstClassTicketCostCalculator implements TicketCostCalculator {

    @Override
    public double calculateTicketCost ( ScheduledFlight flight , SearchCriteria form ) {
        int noOfTravellers = form.getNumberOfPassengers ();
        double ticketCost = 0.0;
        double basePrice = flight.getTravelClassSeatDistributionMap ().get ( form.getTravelClass () ).getBasePrice ();
        Date bookingDate = new Date();
        Date dateOfDeparture = flight.getFlightRoute ().getDepartureDate ();
        long tempDaysDifference = dateOfDeparture.getTime()-bookingDate.getTime();
        long numberOfDaysInbetweenTheTwoGivenDates = TimeUnit.DAYS.convert(tempDaysDifference, TimeUnit.MILLISECONDS);
        numberOfDaysInbetweenTheTwoGivenDates = (numberOfDaysInbetweenTheTwoGivenDates+1);

        for(int i=0;i<noOfTravellers;i++) {
            double newPrice = cost(numberOfDaysInbetweenTheTwoGivenDates,basePrice);
            ticketCost = ticketCost+(newPrice);
        }
        return ticketCost;
    }
    static double cost (float numberOfDaysDifference,double basePrice){
        float diffDate = numberOfDaysDifference;
        float temp = 1-((diffDate)/10);
        DecimalFormat f = new DecimalFormat("##.00");
        double cost = Double.parseDouble(f.format(temp));
        double    newPrice = basePrice+basePrice*cost;
        return newPrice;
    }

    public  Date checkTheDateValidityForFirstClass (Date dateOfDepartureOfFlight){
        //Given Date in String format
        //String oldDate = "2017-01-29";
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);
        String dateOfBooking;
        Date validDateToOpenFirstClass = null;
        Calendar c = Calendar.getInstance();
        try{
            c.setTime(simpleDateFormat.parse(date));
            c.add(Calendar.DAY_OF_MONTH, 10);
            //Date after adding the days to the given date
            dateOfBooking = simpleDateFormat.format(c.getTime());
            validDateToOpenFirstClass = simpleDateFormat.parse ( dateOfBooking );
            System.out.println("Date after Addition: "+validDateToOpenFirstClass);

            //Displaying the new Date after addition of;Days
        }catch(ParseException e){
            e.printStackTrace();
        }
        return validDateToOpenFirstClass;
    }


}

package flight.booking.model;

import java.util.List;
import java.util.Map;

public class AjaxResponseBody {

    private String msg;
    private List <ScheduledFlights_ResultSet> result;
    private List<Map <Integer, String>> sourceAndDestination;

    public void setSourceAndDestination ( List <Map <Integer, String>> sourceAndDestination ) {
        this.sourceAndDestination = sourceAndDestination;
    }

    public List <ScheduledFlights_ResultSet> getResult () {
        return result;
    }

    public void setResult ( List <ScheduledFlights_ResultSet> result ) {
        this.result = result;
    }

    public String getMsg () {
        return msg;
    }

    public void setMsg ( String msg ) {
        this.msg = msg;
    }

}
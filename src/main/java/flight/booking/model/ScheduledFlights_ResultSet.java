package flight.booking.model;

import java.util.Date;

public class ScheduledFlights_ResultSet {
    private String flightNumber;
    private String sourceName;
    private String destinationName;
    private Date flightDepartureDate;
    private int numberOfPassengerTravelling;
    private int availableSeats;
    private String travelClass;
    private double ticketCost;

    public double getTicketCost () {
        return ticketCost;
    }

    public void setTicketCost ( double ticketCost ) {
        this.ticketCost = ticketCost;
    }

    public String getTravelClass () {
        return travelClass;
    }

    public void setTravelClass ( String travelClass ) {
        this.travelClass = travelClass;
    }

    public int getAvailableSeats () {
        return availableSeats;
    }

    public void setAvailableSeats ( int availableSeats ) {
        this.availableSeats = availableSeats;
    }

    public ScheduledFlights_ResultSet () {
    }

    public ScheduledFlights_ResultSet ( String flightNumber , int availbleSeatsInClassSelected , String sourceName ,
                                        String destinationName ,
                                        Date flightDepartureDate ,
                                        String travelClass ,
                                        int numberOfPassengerTravelling ) {
        this.flightNumber = flightNumber;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.flightDepartureDate = flightDepartureDate;
        this.numberOfPassengerTravelling = numberOfPassengerTravelling;
    }

    public String getFlightNumber () {
        return flightNumber;
    }

    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }

    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public Date getFlightDepartureDate () {
        return flightDepartureDate;
    }

    public void setFlightDepartureDate ( Date flightDepartureDate ) {
        this.flightDepartureDate = flightDepartureDate;
    }

    public int getNumberOfPassengerTravelling () {
        return numberOfPassengerTravelling;
    }

    public void setNumberOfPassengerTravelling ( int numberOfPassengerTravelling ) {
        this.numberOfPassengerTravelling = numberOfPassengerTravelling;
    }
}

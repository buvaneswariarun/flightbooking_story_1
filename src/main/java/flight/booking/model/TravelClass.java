package flight.booking.model;

public class TravelClass {

    private String className;
    private int seatsPerDesign;
    private int availableSeatsForBooking;
    private double basePrice;

    public int getSeatsPerDesign () {
        return seatsPerDesign;
    }

    public void setSeatsPerDesign ( int seatsPerDesign ) {
        this.seatsPerDesign = seatsPerDesign;
    }

    public TravelClass ( String className , int availableSeatsForBooking , double basePrice,int seatsPerDesign) {
        this.className = className;
        this.availableSeatsForBooking = availableSeatsForBooking;
        this.seatsPerDesign = seatsPerDesign;
        this.basePrice = basePrice;
    }

    public String getClassName () {
        return className;
    }

    public void setClassName ( String className ) {
        this.className = className;
    }

    public int getAvailableSeatsForBooking () {
        return availableSeatsForBooking;
    }

    public void setAvailableSeatsForBooking ( int availableSeatsForBooking ) {
        this.availableSeatsForBooking = availableSeatsForBooking;
    }

    public double getBasePrice () {
        return basePrice;
    }

    public void setBasePrice ( double basePrice ) {
        this.basePrice = basePrice;
    }
}

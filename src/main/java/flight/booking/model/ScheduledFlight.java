package flight.booking.model;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;


public class ScheduledFlight {
    private Aeroplane aeroplane;
    private String flightNumber;
    private String carrierName;
    private FlightRoute flightRoute_instance;
    private Map <String, TravelClass> travelClassSeatDistributionMap;

    public Map <String, TravelClass> getTravelClassSeatDistributionMap () {
        return travelClassSeatDistributionMap;
    }

    public void setTravelClassSeatDistributionMap ( Map <String, TravelClass> travelClassSeatDistributionMap ) {
        this.travelClassSeatDistributionMap = travelClassSeatDistributionMap;
    }

    public FlightRoute getFlightRoute_instance () {
        return flightRoute_instance;
    }

    public void setFlightRoute_instance ( FlightRoute flightRoute_instance ) {
        this.flightRoute_instance = flightRoute_instance;
    }
    @Autowired
    public ScheduledFlight ( String flightNumber ,
                             String carrierName,
                             FlightRoute flightRoute_instance,
                             Aeroplane aeroplane
                    ) {
        this.setFlightNumber (flightNumber);
        this.setFlightRoute (flightRoute_instance );
        this.setAeroplane ( aeroplane );

    }

    public ScheduledFlight ( String flightNumber ,
                             String carrierName ,
                             FlightRoute flightRoute_instance ,
                             Aeroplane aeroplane ,
                             Map <String, TravelClass> travelClassSeatDistributionMap ) {
        this.aeroplane = aeroplane;
        this.flightNumber = flightNumber;
        this.carrierName = carrierName;
        this.flightRoute_instance = flightRoute_instance;
        this.travelClassSeatDistributionMap = travelClassSeatDistributionMap;
    }

    public ScheduledFlight () {
    }
    public String getCarrierName () {
        return carrierName;
    }

    public void setCarrierName ( String carrierName ) {
        this.carrierName = carrierName;
    }

    public FlightRoute getFlightRoute () {
        return flightRoute_instance;
    }

    public void setFlightRoute ( FlightRoute flightRoute_instance ) {
        this.flightRoute_instance = flightRoute_instance;
    }

    public Aeroplane getAeroplane () {
        return aeroplane;
    }

    public void setAeroplane ( Aeroplane aeroplane ) {
        this.aeroplane = aeroplane;
    }

    public String getFlightNumber () {
        return flightNumber;
    }

    public void setFlightNumber ( String flightNumber ) {
        this.flightNumber = flightNumber;
    }


//    @Override
//    public String toString() {
//        return "Flight{" +
//                "Model='" + flightModel + '\'' +
//                ", Number ='" + flightNumber + '\'' +
//                ", Source   ='" + sourceName + '\'' +
//                ", Destination   ='" + destinationName + '\'' +
//                '}';
//    }

}

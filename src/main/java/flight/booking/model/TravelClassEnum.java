package flight.booking.model;

public enum TravelClassEnum {
    BUSINESS,
    ECONOMY,
    FIRST

}

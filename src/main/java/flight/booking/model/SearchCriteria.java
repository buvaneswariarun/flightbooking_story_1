package flight.booking.model;

import javax.validation.constraints.NotEmpty;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SearchCriteria {
    private String sourceName;
    private String destinationName;
    private int numberOfPassengers;
    private Date dateOfTravel;
    private String travelClass;

    public String getTravelClass () {
        return travelClass;
    }

    public void setTravelClass ( String travelClass ) {
        this.travelClass = travelClass;
    }

    public Date getDateOfTravel () {
        return dateOfTravel;
    }

    public void setDateOfTravel ( Date dateOfTravel )  {
        this.dateOfTravel = dateOfTravel;

    }

    public int getNumberOfPassengers () {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers ( int numberOfPassengers ) {
        this.numberOfPassengers = numberOfPassengers;
    }


    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public static Date makeDate(Date inputDate){
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String newDate = simpleDateFormat.format(new Date());
        Date result;
        try {
            result = simpleDateFormat.parse (newDate  );
            return result;
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        return null;
    }

}


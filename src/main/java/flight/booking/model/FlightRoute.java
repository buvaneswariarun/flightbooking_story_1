package flight.booking.model;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Time;
import java.time.Duration;
import java.util.Date;

public class FlightRoute {
    private String routeID;
    private String sourceName;
    private String destinationName;
    private Date departureDate;
    private Date departureTime;
    private Date arrivalDate;

    public String getRouteID () {
        return routeID;
    }

    public void setRouteID ( String routeID ) {
        this.routeID = routeID;
    }

    public FlightRoute () {

    }

    public String getSourceName () {
        return sourceName;
    }

    public void setSourceName ( String sourceName ) {
        this.sourceName = sourceName;
    }

    public String getDestinationName () {
        return destinationName;
    }

    public void setDestinationName ( String destinationName ) {
        this.destinationName = destinationName;
    }

    public Date getDepartureDate () {
        return departureDate;
    }

    public void setDepartureDate ( Date departureDate ) {
        this.departureDate = departureDate;
    }

    public Date getFlightDuration () {
        return departureTime;
    }

    public void setFlightDuration ( Date departureTime ) {
        this.departureTime = departureTime;
    }

    public Date getArrivalDate () {
        return arrivalDate;
    }

    public void setArrivalDate ( Date arrivalDate ) {
        this.arrivalDate = arrivalDate;
    }

    public FlightRoute ( String routeID, String sourceName , String destinationName , Date departureDate) {
        this.routeID= routeID;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.departureDate = departureDate;
    }

    public Date getDepartureTime () {
        return departureTime;
    }

    public void setDepartureTime ( Date departureTime ) {
        this.departureTime = departureTime;
    }

    @Autowired
    public FlightRoute ( String routeID,
                         String sourceName ,
                         String destinationName ,
                         Date departureDate ,
                         Date departureTime ,
                         Date arrivalDate ) {
        this.routeID = routeID;
        this.sourceName = sourceName;
        this.destinationName = destinationName;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.arrivalDate = arrivalDate;
    }
}

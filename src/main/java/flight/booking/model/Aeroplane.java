package flight.booking.model;

import java.util.Map;


public class Aeroplane {

    private String aeroplaneName;
    private Map<String,Integer> seatDistribution;
    private int totalSeatsAsPerDesign;

    public Map <String, Integer> getSeatDistribution () {
        return seatDistribution;
    }

    public void setSeatDistribution ( Map <String, Integer> seatDistribution ) {
        this.seatDistribution = seatDistribution;
    }

    public int getTotalSeatsAsPerDesign () {
        return totalSeatsAsPerDesign;
    }

    public void setTotalSeatsAsPerDesign ( int totalSeatsAsPerDesign ) {
        this.totalSeatsAsPerDesign = totalSeatsAsPerDesign;
    }

    public Map <String, Integer> getTravelClassModelMap () {
        return seatDistribution;
    }
    public void setTravelClassModelMap ( Map <String, Integer> travelClassModelMap ) {
        this.seatDistribution = seatDistribution;
    }

    public Aeroplane () {
    }
    public String getAeroplaneName () {
        return aeroplaneName;
    }

    public void setAeroplaneName ( String aeroplaneName ) {
        this.aeroplaneName = aeroplaneName;
    }

    public Aeroplane (
            String aeroplaneName ,
            Map<String,Integer> seatDistribution,
            int totalSeatsAsPerDesign )
    {
        this.aeroplaneName = aeroplaneName;
        this.seatDistribution = seatDistribution;
        this.totalSeatsAsPerDesign = totalSeatsAsPerDesign;

    }




}

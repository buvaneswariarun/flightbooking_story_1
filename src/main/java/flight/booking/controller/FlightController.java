package flight.booking.controller;

import flight.booking.model.AjaxResponseBody;
import flight.booking.model.ScheduledFlight;
import flight.booking.model.SearchCriteria;
import flight.booking.model.ScheduledFlights_ResultSet;
import flight.booking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class FlightController {

@Autowired
FlightService service;

    @RequestMapping("/find")
    @CrossOrigin
    public String findFlight(){
        return "AEROPLANE";
    }

    @RequestMapping("/search")
    @CrossOrigin
    public ArrayList <ScheduledFlight> getAllFlights () {
        return (service.searchFlight ());
    }

    @RequestMapping("/flight/{flightNumber}")
    public ScheduledFlight getFlightByNumber ( @PathVariable String flightNumber ) {
        return (service.getFlightByFlightNumber ( flightNumber ));
    }

    @PostMapping("/api/searchWithDate")
    public ResponseEntity <?> getSearchResultViaAjax ( @Valid @RequestBody SearchCriteria searchCriteria , Errors errors ) {

        AjaxResponseBody result = new AjaxResponseBody ();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors ()) {
            result.setMsg ( errors.getAllErrors ()
                    .stream (). <String>map ( x -> x.getDefaultMessage () )
                    .collect ( Collectors.joining ( "," ) ) );
            return ResponseEntity.badRequest ().body ( result );
        }
        List <ScheduledFlights_ResultSet> flights = service.searchFlightBasedOnSourceAndDestinationNew ( searchCriteria );
        if (flights.isEmpty ()) {
            result.setMsg ( "no flights found!" );
        } else {
            result.setMsg ( "success" );
        }
        result.setResult ( flights );

        return ResponseEntity.ok ( result );

    }
    @RequestMapping("/sources")
    public ResponseEntity <?> sourceData() {
        List <String> sources = new ArrayList <String> ();
        sources = service.getSources ();

        return ResponseEntity.ok ( sources );
    }
    @RequestMapping("/destinations")
    public ResponseEntity <?> destinationData() {
        List <String> destinations = new ArrayList <String> ();
        destinations = service.getSources ();

        return ResponseEntity.ok ( destinations );
    }
    @RequestMapping("/travelClass")
    public ResponseEntity <?> travelClassData() {
        List <String> travelClass = new ArrayList <String> ();
        travelClass = service.getSources ();

        return ResponseEntity.ok ( travelClass );
    }
}
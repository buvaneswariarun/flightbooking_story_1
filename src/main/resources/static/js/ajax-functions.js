$(document).ready(function () {
$('#sourceName').focus(function() {
    var data = "";
    $.ajax({
        type:"GET",
        url : "/sources",
        data : "sourceName_selectedvalue="+$(this).val(),
        async: false,
        success : function(response) {
            data = response;
            return response;
        },
        error: function() {
            alert('Error occured');
        }
    });

    $.each(data, function(index, value) {
        $('#sourceName').append('<option>' + value + '</option>');
    });
        $('#sourceName').show();
});

$('#destinationName').focus(function() {
    var data = "";
    $.ajax({
        type:"GET",
        url : "/destinations",
        data : "destinationName_selectedvalue="+$(this).val(),
        async: false,
        success : function(response) {
            data = response;
            return response;
        },
        error: function() {
            alert('Error occured');
        }
    });

    $.each(data, function(index, value) {
        $('#destinationName').append('<option>' + value + '</option>');
    });
        $('#destinationName').show();
});

$("#flight-booking-form").submit(function (event) {
        event.preventDefault($( "#dateOfTravel" ).val());
        var flightBookingForm = {}
         var src = $("#sourceName").text();

        if(src.length ===0) {
                        alert('Please fill in Source');
                        return false;
                    }else{
                    flightBookingForm["sourceName"] = $( "#sourceName option:selected" ).text();
                    }
       var dest = $("#destinationName").text();
        if(dest.length===0) {
                                alert('Please fill in Destination');
                                return false;
                        }else{
                            flightBookingForm["destinationName"] = $("#destinationName option:selected").text();
                        }
        var np = $("#numberOfPassengers").val();
        if((np===0)||(np.length===0)){
                 alert('No Passenger selected. Showing result for 1 passenger');
                 flightBookingForm["numberOfPassengers"] = 1;
        }else{
                 flightBookingForm["numberOfPassengers"] = $("#numberOfPassengers").val();
        }
        var date = new Date($( "#dateOfTravel" ).val());
        var jsonDate = date.toJSON();
        flightBookingForm["dateOfTravel"] =jsonDate;
        flightBookingForm["travelClass"]   =$( "#travelClass option:selected" ).text();
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/api/searchWithDate",
            data: JSON.stringify(flightBookingForm),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                    if(data.result.length == 0){
                    alert('NO Flights Found Matching This Criteria. Try Again Please.');
                    }
                    $("#result").find("tbody").empty();
                    var tr;
                    for (var i = 0; i < data.result.length; i++) {
                        tr = $("<tr/>");
                        tr.append("<td>" + data.result[i].flightNumber + "</td>");
                        tr.append("<td>" + data.result[i].sourceName + "</td>");
                        tr.append("<td>" + data.result[i].destinationName + "</td>");
                        tr.append("<td>" + data.result[i].flightDepartureDate + "</td>");
                        tr.append("<td>" + data.result[i].availableSeats+ "</td>");
                        tr.append("<td>" + data.result[i].travelClass + "</td>");
                         tr.append("<td>" + data.result[i].ticketCost + "</td>");
                        $("#result").find("tbody").append(tr);
                    }
                      $('#feedback').html(json);

                 console.log("SUCCESS : ", data);
                    $("#btn-search").prop("enabled", true);
                    },

            error: function (e) {

                var json = "<h4>Ajax Response Error</h4><pre>"
                    + e.responseText + "</pre>";
                $('#feedback').html(json);

                console.log("ERROR : ", e);
                $("#btn-search").prop("disabled", false);

            }
        });

    });

});
